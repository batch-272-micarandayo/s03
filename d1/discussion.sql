[SECTION] Inserting Records

--inserting data to a particular table and 1 column
INSERT INTO artists (name) VALUES ("Rivermaya");
INSERT INTO artists (name) VALUES ("Psy");

--inserting data to a particular table and/with 2 or more columns
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Psy 6", "2012-1-1", 2);

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Trip", "1996-1-1", 1);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Gangnam Style", 253, "K-pop", 3);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("Ulan", 234, "OPM", 4);

INSERT INTO songs (song_name, length, genre, album_id) VALUES ("214", 249, "OPM", 4);

[SECTION] --READ AND SELECT RECORDS/DATA

-- Display the title and genre of all the songs 
SELECT song_name, genre FROM songs;

-- Display the song name of all the OPM songs.
SELECT song_name FROM songs WHERE genre = "OPM";

-- Display all data with all attributes in the songs table
SELECT * FROM songs;

-- Display the title and length of the OPM songs that are more than 2 minutes and 1 second
SELECT song_name, length FROM songs WHERE length > 201 AND genre = "OPM";

-- Display the title and length of the OPM songs that are more than 2 minutes and 30 seconds
SELECT song_name, length FROM songs WHERE length > 235 AND genre = "K-pop";

[SECTION] Updating Records

UPDATE songs SET length = 307 WHERE song_name = "214";

UPDATE songs SET song_name = "Ulan Updated" WHERE song_name = "Ulan";

[SECTION]

-- Delete a song
DELETE FROM songs WHERE genre = "OPM" AND length > 255;

-- Delete all from a table
DELETE * FROM songs;